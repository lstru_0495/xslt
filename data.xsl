<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output
method="xml"
version="1.0"
encoding="UTF-8"
omit-xml-declaration="no"
standalone="no"
cdata-section-elements="namelist"
indent="yes"/> 

  <xsl:template match ="records">
    <users>
      <xsl:apply-templates select= "record"/>
    </users>
  </xsl:template>

  <xsl:template match = "record">
    <user>
      <firstname>
        <xsl:value-of select ="firstname"/>
      </firstname>

      <lastname>
        <xsl:value-of select ="lastname"/>
      </lastname>

      <email>
        <xsl:value-of select ="email"/>
      </email>

      <phone>
        <xsl:value-of select ="phone"/>
      </phone>

      <country>
        <xsl:value-of select ="country"/>
      </country>

      <zipcode>
        <xsl:value-of select ="zipcode"/>
      </zipcode>

    </user>

  </xsl:template>
</xsl:stylesheet>

