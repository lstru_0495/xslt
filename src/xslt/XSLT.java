package xslt;


import javax.xml.crypto.dsig.TransformException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
public class XSLT {

    public static void main(String[] args) {
        TransformerFactory factory = TransformerFactory.newInstance();
        String xsltTemplate = "data.xsl";
        StreamSource xslStream = new StreamSource(xsltTemplate);
        
        String dataXML = "data.xml";
        StreamSource inStream = new StreamSource(dataXML);
        StreamResult outStream = new StreamResult("out.xml");
        
        try {
            Transformer transformer =  factory.newTransformer(xslStream);
            transformer.transform(inStream, outStream);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
    
}
