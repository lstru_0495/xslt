<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output
method="xml"
version="1.0"
encoding="UTF-8"
omit-xml-declaration="no"
standalone="no"
cdata-section-elements="namelist"
indent="yes"/> 

  <xsl:template match ="users">
    <beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="
                http://www.springframework.org/schema/beans
                http://www.springframework.org/schema/beans/spring-beans.xsd">
      <xsl:apply-templates select= "user"/>
    </beans>
  </xsl:template>

  <xsl:template match="user">
    <bean>
      <xsl:attribute name="id">
        <xsl:value-of select="count(preceding::user)+1"/>
      </xsl:attribute>

      <xsl:attribute name="class">
        <xsl:value-of select="'com.verification.User'"/>
      </xsl:attribute>

      <property>
        <xsl:attribute name="name">
          <xsl:value-of select="'firstName'"/>
        </xsl:attribute>
        <xsl:attribute name ="value">
          <xsl:value-of select ="firstname"/>
        </xsl:attribute>
      </property>

      <property>
        <xsl:attribute name="name">
          <xsl:value-of select="'lastName'"/>
        </xsl:attribute>
        <xsl:attribute name ="value">
          <xsl:value-of select ="lastname"/>
        </xsl:attribute>
      </property>

      <property>
        <xsl:attribute name="name">
          <xsl:value-of select="'email'"/>
        </xsl:attribute>
        <xsl:attribute name ="value">
          <xsl:value-of select ="email"/>
        </xsl:attribute>
      </property>

      <property>
        <xsl:attribute name="name">
          <xsl:value-of select="'phone'"/>
        </xsl:attribute>
        <xsl:attribute name ="value">
          <xsl:value-of select ="phone"/>
        </xsl:attribute>
      </property>

      <property>
        <xsl:attribute name="name">
          <xsl:value-of select="'zip'"/>
        </xsl:attribute>
        <xsl:attribute name ="value">
          <xsl:value-of select ="zipcode"/>
        </xsl:attribute>
      </property>
    </bean>
  </xsl:template>

</xsl:stylesheet>

